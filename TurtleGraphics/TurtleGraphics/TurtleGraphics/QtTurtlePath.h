#ifndef QTTURTLEPATH_H
#define QTTURTLEPATH_H

#include <QGraphicsPathItem>

class QtTurtlePath : public QGraphicsPathItem
{
	private:
		QPointF m_startPos;
	public:
		QtTurtlePath(const QPointF& pos);

		void moveTo(const QPointF& pos);
		void lineTo(const QPointF& pos);
		void reset();

		float getPathLength() const;
};

#endif // QTTURTLEPATH_H
