/********************************************************************************
** Form generated from reading UI file 'RuleDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RULEDIALOG_H
#define UI_RULEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_RuleDialog
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *labelSymbol;
    QComboBox *comboBoxSymbol;
    QLineEdit *lineEditReplacementString;
    QLabel *labelSymbolString;
    QGroupBox *groupBox;
    QComboBox *comboBoxCommand;
    QComboBox *comboBoxEnum;
    QLineEdit *lineEditValue;

    void setupUi(QDialog *RuleDialog)
    {
        if (RuleDialog->objectName().isEmpty())
            RuleDialog->setObjectName(QStringLiteral("RuleDialog"));
        RuleDialog->resize(309, 300);
        buttonBox = new QDialogButtonBox(RuleDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 240, 291, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        labelSymbol = new QLabel(RuleDialog);
        labelSymbol->setObjectName(QStringLiteral("labelSymbol"));
        labelSymbol->setGeometry(QRect(10, 10, 47, 13));
        comboBoxSymbol = new QComboBox(RuleDialog);
        comboBoxSymbol->setObjectName(QStringLiteral("comboBoxSymbol"));
        comboBoxSymbol->setGeometry(QRect(10, 30, 141, 22));
        lineEditReplacementString = new QLineEdit(RuleDialog);
        lineEditReplacementString->setObjectName(QStringLiteral("lineEditReplacementString"));
        lineEditReplacementString->setGeometry(QRect(172, 30, 121, 20));
        labelSymbolString = new QLabel(RuleDialog);
        labelSymbolString->setObjectName(QStringLiteral("labelSymbolString"));
        labelSymbolString->setGeometry(QRect(170, 10, 91, 16));
        groupBox = new QGroupBox(RuleDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 100, 281, 101));
        comboBoxCommand = new QComboBox(groupBox);
        comboBoxCommand->setObjectName(QStringLiteral("comboBoxCommand"));
        comboBoxCommand->setGeometry(QRect(20, 30, 161, 22));
        comboBoxEnum = new QComboBox(groupBox);
        comboBoxEnum->setObjectName(QStringLiteral("comboBoxEnum"));
        comboBoxEnum->setGeometry(QRect(20, 70, 121, 22));
        lineEditValue = new QLineEdit(groupBox);
        lineEditValue->setObjectName(QStringLiteral("lineEditValue"));
        lineEditValue->setGeometry(QRect(150, 70, 113, 20));

        retranslateUi(RuleDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), RuleDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), RuleDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(RuleDialog);
    } // setupUi

    void retranslateUi(QDialog *RuleDialog)
    {
        RuleDialog->setWindowTitle(QApplication::translate("RuleDialog", "Dialog", 0));
        labelSymbol->setText(QApplication::translate("RuleDialog", "Symbol:", 0));
        labelSymbolString->setText(QApplication::translate("RuleDialog", "Replacement string:", 0));
        groupBox->setTitle(QApplication::translate("RuleDialog", "GroupBox", 0));
        comboBoxCommand->clear();
        comboBoxCommand->insertItems(0, QStringList()
         << QApplication::translate("RuleDialog", "Null", 0)
         << QApplication::translate("RuleDialog", "Stack: push position   ", 0)
         << QApplication::translate("RuleDialog", "Stack: pop position", 0)
         << QApplication::translate("RuleDialog", "Move: forward", 0)
         << QApplication::translate("RuleDialog", "Rotate: turn anti-clockwise", 0)
         << QApplication::translate("RuleDialog", "Rotate: turn clockwise ", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class RuleDialog: public Ui_RuleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RULEDIALOG_H
