#ifndef COMMANDS_DIALOG_H
#define COMMANDS_DIALOG_H

#include <QDialog>

namespace Ui {
class CommandsDialog;
}

class CommandsDialog : public QDialog
{
	Q_OBJECT

public:
	explicit CommandsDialog(QWidget *parent = 0);
	~CommandsDialog();

private:
	Ui::CommandsDialog *ui;
};

#endif // COMMANDS_DIALOG_H
