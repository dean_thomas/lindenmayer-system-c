/********************************************************************************
** Form generated from reading UI file 'TurtleGraphicsWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TURTLEGRAPHICSWIDGET_H
#define UI_TURTLEGRAPHICSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TurtleGraphicsWidget
{
public:

    void setupUi(QWidget *TurtleGraphicsWidget)
    {
        if (TurtleGraphicsWidget->objectName().isEmpty())
            TurtleGraphicsWidget->setObjectName(QStringLiteral("TurtleGraphicsWidget"));
        TurtleGraphicsWidget->resize(400, 300);

        retranslateUi(TurtleGraphicsWidget);

        QMetaObject::connectSlotsByName(TurtleGraphicsWidget);
    } // setupUi

    void retranslateUi(QWidget *TurtleGraphicsWidget)
    {
        TurtleGraphicsWidget->setWindowTitle(QApplication::translate("TurtleGraphicsWidget", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class TurtleGraphicsWidget: public Ui_TurtleGraphicsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TURTLEGRAPHICSWIDGET_H
