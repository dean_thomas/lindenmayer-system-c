#include "QtTurtleCursor.h"
#include "CommandInterface/ITurtleCommand.h"
#include <QPainter>
#include <iostream>
#include <cassert>
#include <cmath>

using namespace std;

std::ostream& operator<<(std::ostream& os, const QtTurtleCursor& cursor)
{
	const size_t TABS = 1;

	auto tabPrinter = [&](std::ostream& os, size_t tabs)
	{
		for (auto i = (size_t)0; i < tabs; ++i)
		{
			os << '\t';
		}
	};

	os << "QtTurtleCursor" << endl << "{" << endl;
	tabPrinter(os, TABS); os << "pos = (" << cursor.pos().x() << ", " << cursor.pos().y() << ")" << endl;
	tabPrinter(os, TABS); os << "rotation = (" << cursor.rotation() << ")" << endl;
	tabPrinter(os, TABS); os << "angle = " << cursor.m_angle << endl;
	tabPrinter(os, TABS); os << "pen is " << (cursor.m_penDown ? "down" : "up") << endl;

	os << "}";
	return os;
}

QtTurtleCursor::QtTurtleCursor(const QPointF &pos)
	: m_startPos{pos}
{
	setPos(pos);
	QRectF rect = boundingRect();

	setTransformOriginPoint(rect.center());
	m_outlinePath.addEllipse(rect);
}

QRectF QtTurtleCursor::boundingRect() const
{
	return QRectF(QPointF(-RADIUS, -RADIUS), QPointF(+RADIUS, +RADIUS));
}

void QtTurtleCursor::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
	//setRotation(m_angle);

	QRectF rect = boundingRect();

	const QColor& brushColour = (m_penDown ? COLOUR_PEN_DOWN : COLOUR_PEN_UP);
	m_brush.setColor(brushColour);
	painter->fillPath(m_outlinePath, brushColour);

	painter->drawPath(m_outlinePath);
	//painter->rotate(m_angle);
	painter->drawLine(rect.center(), QPointF(rect.center().x(), rect.center().y() + RADIUS));
}

void QtTurtleCursor::turnClockwise(const float degrees)
{
	setRotation(rotation() + degrees);
	//m_angle += degrees;

	while(m_angle >= 360.0f) m_angle -= 360.0f;

	assert(0.0f <= m_angle && m_angle < 360.f);

	//cout << *this << endl;

	update();
}

void QtTurtleCursor::turnAntiClockwise(const float degrees)
{
	setRotation(rotation() - degrees);
	//m_angle -= degrees;

	while(m_angle < 0.0f) m_angle += 360.0f;

	assert(0.0f <= m_angle && m_angle < 360.f);
	//cout << *this << endl;

	update();
}

void QtTurtleCursor::penUp()
{
	m_penDown = false;
	//cout << *this << endl;

	update();
}

void QtTurtleCursor::penDown()
{
	m_penDown = true;
	//cout << *this << endl;

	update();
}

void QtTurtleCursor::penToggle()
{
	m_penDown = !m_penDown;
	//cout << *this << endl;

	update();
}

void QtTurtleCursor::move(const float distance)
{
	auto currentPos = pos();
	auto dx = -(distance * sin(TO_RADS(rotation())));
	auto dy = distance * cos(TO_RADS(rotation()));

	setPos(currentPos.x() + dx, currentPos.y() + dy);
	//cout << *this << endl;

	update();
}

void QtTurtleCursor::moveBack(const float distance)
{
	move(-distance);

	update();
}

void QtTurtleCursor::moveForward(const float distance)
{
	move(distance);

	update();
}

void QtTurtleCursor::push()
{
	m_stack.push({pos(), rotation()});
}

void QtTurtleCursor::pop()
{
	assert(!m_stack.empty());

	StackEntry top = m_stack.pop();

	//m_previousPos = m_currentPos;
	setPos(top.pos);
	setRotation(top.angle);

	update();
}

void QtTurtleCursor::reset()
{
	setPos(m_startPos);
	//m_previousPos = m_currentPos;
	setRotation(0.0f);
	//m_pen = QPen(QColor(Qt::black));
	m_penDown = true;
	m_stack.clear();
}
