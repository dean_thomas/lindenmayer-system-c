#ifndef RULEDIALOG_H
#define RULEDIALOG_H

#include <QDialog>
#include <vector>
#include "LSystem/LSystem.h"
#include <cassert>

using std::vector;

namespace Ui {
class RuleDialog;
}

class RuleDialog : public QDialog
{
    Q_OBJECT

    LSystem m_lSystem;
public:
    explicit RuleDialog(const LSystem &lSystem, QWidget *parent = 0);

    char GetSelectedCharacter() const;
    string GetReplacementString() const;

    ~RuleDialog();

private:
    Ui::RuleDialog *ui;
};

#endif // RULEDIALOG_H
