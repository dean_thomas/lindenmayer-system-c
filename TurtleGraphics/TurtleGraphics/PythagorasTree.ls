#   Comments start with a hash symbol
#
#   Created on 04-05-2016
#
#   The title will indicate what is display in the user interface
title:  Pythagoras Tree
#   The axiom represents the start state of the system
axiom:  0

#   Replacement rules are represented one line per rule (and are case sensitive)
0:  1[0]0
1:  11
