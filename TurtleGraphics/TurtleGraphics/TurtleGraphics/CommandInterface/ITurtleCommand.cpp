#include "ITurtleCommand.h"
#include <iostream>

using namespace std;

ITurtleCommand::~ITurtleCommand()
{
	cout << "Calling destructor: " << __func__ << ".\n";
	fflush(stdout);
}

QVector<QLineF>  NullCommand::Execute(const QVector<QLineF> &path) const
{
	return path;
}

MacroCommand::MacroCommand(PtrCommand &&existingCommand, PtrCommand &&newCommand)
{
	cout << "Creating a macro command.\n";
	fflush(stdout);

	//  TODO: currently macro commands will be added as macros.
	//  It may be better to strip each command from the input
	//  macro and add directly to the m_commands list at a later
	//  date.
	m_commands.push_back(std::move(existingCommand));
	m_commands.push_back(std::move(newCommand));
}

string NullCommand::ToString() const
{
	stringstream result;

	result << "<command class='null' />";

	return result.str();
}

QVector<QLineF> MacroCommand::Execute(const QVector<QLineF> &path) const
{
	QVector<QLineF> tempPath(path);

	for (CommandList::const_iterator it = m_commands.begin();
		 it != m_commands.end(); ++it)
	{
		tempPath = (*it)->Execute(tempPath);
	}
	return tempPath;
}

string MacroCommand::ToString() const
{
	stringstream result;

	result << "<command class='macro'>\n";
	for (CommandList::const_iterator it = m_commands.begin();
		 it != m_commands.end(); ++it)
	{
		result << "\t" << (*it)->ToString() << "\n";
	}
	result << "</command>\n";

	return result.str();
}

MacroCommand::~MacroCommand()
{
	cout << "Calling destructor: " << __func__ << ".\n";
	fflush(stdout);
}

QVector<QLineF> RotateCommand::Execute(const QVector<QLineF> &path) const
{
	assert(m_turtle != nullptr);
/*
	switch (m_action)
	{
	case Action::CLOCKWISE:
		m_turtle->TurnClockwise(m_degrees);
		break;
	case Action::ANTI_CLOCKWISE:
		m_turtle->TurnAntiClockwise(m_degrees);
		break;
	}
	return path;
	*/
	return QVector<QLineF>();
}

string RotateCommand::ToString() const
{
	stringstream result;

	result << "<command class='rotate' action='";
	result << (m_action==Action::CLOCKWISE ? "CW" : "CCW");
	result << "' degrees='" << m_degrees << "' />";

	return result.str();
}

RotateCommand::~RotateCommand()
{
	cout << "Calling destructor: " << __func__ << ".\n";
	fflush(stdout);
}

QVector<QLineF> MoveCommand::Execute(const QVector<QLineF> &path) const
{
	assert(m_turtle != nullptr);
/*
	m_turtle->MoveForward(m_distance);

	if (m_turtle->IsPenDown())
	{
		QVector<QLineF> tempPath(path);

		tempPath.push_back(QLineF(m_turtle->GetPreviousPosition(),
								  m_turtle->GetCurrentPosition()));

		return tempPath;
	}
	else
	{
		return path;
	}
	*/
	return QVector<QLineF>();
}

string MoveCommand::ToString() const
{
	stringstream result;

	result << "<command class='move' ";
	result << "distance='" << m_distance << "'";
	result << " />";

	return result.str();
}

MoveCommand::~MoveCommand()
{
	cout << "Calling destructor: " << __func__ << ".\n";
	fflush(stdout);
}

QVector<QLineF> StackCommand::Execute(const QVector<QLineF> &path) const
{
	assert(m_turtle != nullptr);
/*
	switch (m_action)
	{
	case Action::PUSH:
		m_turtle->Push();
		break;
	case Action::POP:
		m_turtle->Pop();
		break;
	}
	return path;
	*/
	return QVector<QLineF>();
}

string StackCommand::ToString() const
{
	stringstream result;

	result << "<command class='stack' ";
	result << "action='" << (m_action==Action::PUSH ? "push" : "pop");
	result << "' />";

	return result.str();
}

StackCommand::~StackCommand()
{
	cout << "Calling destructor: " << __func__ << ".\n";
	fflush(stdout);
}
