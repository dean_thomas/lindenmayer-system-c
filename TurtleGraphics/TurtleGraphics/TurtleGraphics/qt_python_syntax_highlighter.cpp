#include "qt_python_syntax_highlighter.h"
#include <QDebug>

QtPythonSyntaxHighlighter::QtPythonSyntaxHighlighter(QObject* parent)
	: QSyntaxHighlighter(parent)
{
	init();
}

QtPythonSyntaxHighlighter::QtPythonSyntaxHighlighter(QTextDocument *parent)
	: QSyntaxHighlighter(parent)
{
	init();
}


bool QtPythonSyntaxHighlighter::match_multi_line(const QString& text, const Rule& rule)
{
	int start;
	int add;

	//	Unpack struct
	auto& delimiter = rule.expression;
	auto& in_state = rule.flag;
	auto& style = rule.style;

	//	If inside triple-single quotes, start at 0
	if (previousBlockState() == in_state)
	{
		start = 0;
		add = 0;
	}
	//	Otherwise, look for the delimiter on this line
	else
	{
		start = delimiter.indexIn(text);
		//	Move past this match
		add = delimiter.matchedLength();
	}

	//	As long as there's a delimiter match on this line...
	while (start >= 0)
	{
		//	Look for the ending delimiter
		auto end = delimiter.indexIn(text, start + add);
		auto length = 0ul;

		//	Ending delimiter on this line?
		if (end >= add)
		{
			length = end - start + add + delimiter.matchedLength();
			setCurrentBlockState(0);
		}
		// No; multi-line string
		else
		{
			setCurrentBlockState(in_state);
			length = text.length() - start + add;
		}

		//	Apply formatting
		setFormat(start, length, style);

		//	Look for the next match
		start = delimiter.indexIn(text, start + length);
	}

	//	Return True if still inside a multi-line string, False otherwise
	if (currentBlockState() == in_state)
		return true;
	else
		return false;
}

void QtPythonSyntaxHighlighter::highlightBlock(const QString& text)
{
	auto x = 0ul;

	for (auto& rule : m_rules)
	{
		qDebug() << x << " : " << m_rules.at(x).expression.pattern();
		++x;

		//expression, nth, format
		auto& expression = rule.expression;
		auto& nth = rule.flag;
		auto& format = rule.style;

		auto index = expression.indexIn(text, 0);

		while (index >= 0)
		{
			//qDebug() << "Found match";

			// We actually want the index of the nth match
			index = expression.pos(nth);
			auto length = expression.cap(nth).length();

			setFormat(index, length, format);
			index = expression.indexIn(text, index + length);
		}
	}

	setCurrentBlockState(0);

	// Do multi-line strings
	auto in_multiline = match_multi_line(text, m_tri_single);

	if (!in_multiline)
		in_multiline = match_multi_line(text, m_tri_double);

	qDebug() << "Exiting highlight func";
}

void QtPythonSyntaxHighlighter::init()
{
	//	Set up the various styles to be used
	m_styles["keyword"] = format("blue");
	m_styles["operator"] = format("red");
	m_styles["brace"] = format("darkGray");
	m_styles["defclass"] = format("black", "bold");
	m_styles["string"] = format("magenta");
	m_styles["string2"] = format("darkMagenta");
	m_styles["comment"] = format("darkGreen", "italic");
	m_styles["self"] = format("black", "italic");
	m_styles["numbers"] = format("brown");

	//	Setup keywords for the language
	m_keywords = { "and", "assert", "break", "class", "continue", "def",
				   "del", "elif", "else", "except", "exec", "finally",
				   "for", "from", "global", "if", "import", "in",
				   "is", "lambda", "not", "or", "pass", "print",
				   "raise", "return", "try", "while", "yield",
				   "None", "True", "False" };

	qDebug() << "Keyword count: " << m_keywords.size();

	//	Setup operators for the language
	m_operators = { "=",
					"==", "!=", "<", "<=", ">", ">=",
					"\\+", "-", "\\*", "/", "//", "\\%", "\\*\\*",
					"\\+=", "-=", "\\*=", "/=", "\\%=",
					"\\^", "\\|", "\\&", "\\~",
					">>", "<<"
				  };

	qDebug() << "Operator count: " << m_operators.size();

	//	Setup bracket symbols for the language
	m_braces = { "\\{", "\\}",
				 "\\(", "\\)",
				 "\\[", "\\]"
			   };

	qDebug() << "Braces count: " << m_braces.size();

	//	Setup rules
	for (auto& kw : m_keywords)
	{
		auto style = m_styles.at("keyword");
		Rule newRule = { QRegExp(QString("\\b%1\\b").arg(QString::fromStdString(kw))), 0, style };
		m_rules.push_back(newRule);
	}
	for (auto& op : m_operators)
	{
		auto style = m_styles.at("operator");
		Rule newRule = { QRegExp(QString::fromStdString(op)), 0, style };
		m_rules.push_back(newRule);
	}
	for (auto& br : m_braces)
	{
		auto style = m_styles.at("brace");
		Rule newRule = { QRegExp(QString::fromStdString(br)), 0, style };
		m_rules.push_back(newRule);
	}

	//	All other rules
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\bself\b)|")), 0, m_styles.at("self") });
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|("[^"\\]*(\\.[^"\\]*)*")|")), 0, m_styles.at("string")});

	//  Single-quoted string, possibly containing escape sequences
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|('[^'\\]*(\\.[^'\\]*)*')|")), 0, m_styles.at("string")});
	//  'def' followed by an identifier
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\bdef\b\s*(\w+))|")), 1, m_styles.at("defclass")});
	//  'class' followed by an identifier
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\bclass\b\s*(\w+))|")), 1, m_styles.at("defclass")});
	//  From '#' until a newline
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(#[^\n]*)|")), 0, m_styles.at("comment")});
	//  Numeric literals
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\b[+-]?[0-9]+[lL]?\b)|")), 0, m_styles.at("numbers")});
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b)|")), 0, m_styles.at("numbers")});
	m_rules.push_back( { QRegExp(QString::fromStdString(R"|(\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b)|")), 0, m_styles.at("numbers")});

	qDebug() << "Finished initialising syntax highlighter";

	qDebug() << "Number of rules: " << m_rules.size();

	m_tri_single = { QRegExp(QString::fromStdString(R"|(''')|")), 1, m_styles.at("string2") };
	m_tri_double = { QRegExp(QString::fromStdString(R"|(""")|")), 2, m_styles.at("string2") };

}

QTextCharFormat QtPythonSyntaxHighlighter::format(const QString& color, const QString& style)
{
	QColor colour;
	colour.setNamedColor(color);

	QTextCharFormat result;
	result.setForeground(colour);

	if (style.contains("bold", Qt::CaseInsensitive))
		result.setFontWeight(QFont::Bold);
	if (style.contains("italic", Qt::CaseInsensitive))
		result.setFontItalic(true);

	return result;
}
