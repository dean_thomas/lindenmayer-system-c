#ifndef TURTLEGRAPHICSWIDGET_H
#define TURTLEGRAPHICSWIDGET_H

#include "TurtleGraphics/python_interface.h"

#include <QGraphicsView>
#include <QWidget>
#include <QPainter>
#include <QVector2D>

#include <memory>

#include "QtTurtleCursor.h"
#include "QtTurtlePath.h"

#include "CommandInterface/CommandFactory.h"


class QGraphicsScene;
class QWheelEvent;

namespace Ui {
class TurtleGraphicsWidget;
}

class TurtleGraphicsWidget : public QGraphicsView
{
	Q_OBJECT

public:
	//QPointF
	PythonInterface* m_python_interface = nullptr;


	explicit TurtleGraphicsWidget(QWidget *parent = 0);
	~TurtleGraphicsWidget();

	void wheelEvent(QWheelEvent* event) override;

	QtTurtleCursor* m_turtleCursor = nullptr;
	QtTurtlePath* m_turtlePath = nullptr;

	QGraphicsScene* m_graphicsScene = nullptr;

	void execute_python_expression(const std::string& expression);

	void rotateCursorPos(const float degrees);
	void rotateCursorNeg(const float degrees);

	void penUp();
	void penDown();
	void penToggle();

	void moveForward(const float distance);
	void moveBack(const float distance);
	void move(const float distance);

	void push();
	void pop();

	void reset();

	bool isPenDown() const { return m_turtleCursor->isPenDown(); }
	size_t getStackSize() const { return m_turtleCursor->getStackSize(); }
	float getCurrentAngle() const { return m_turtleCursor->getCurrentAngle(); }
	QPointF getCurrentPosition() const { return m_turtleCursor->getCurrentPosition(); }
	QVector2D getVectorToOrigin() const { return m_turtleCursor->getVectorToOrigin(); }

	float getPathLength() const { return m_turtlePath->getPathLength(); }

private:
	float m_scale = 1.0f;

	Ui::TurtleGraphicsWidget *ui;
};

#endif // TURTLEGRAPHICSWIDGET_H
