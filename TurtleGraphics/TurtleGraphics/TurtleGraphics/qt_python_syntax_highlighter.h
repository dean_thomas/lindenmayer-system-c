#ifndef QTPYTHONSYNTAXHIGHLIGHTER_H
#define QTPYTHONSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QObject>
#include <QTextDocument>
#include <QRegExp>

#include <map>
#include <string>
#include <vector>

//	Basic implementation adapted from:
//	https://wiki.python.org/moin/PyQt/Python%20syntax%20highlighting
//	accessed [27-12-2016]
class QtPythonSyntaxHighlighter : public QSyntaxHighlighter
{
private:
	struct Rule
	{
		QRegExp expression;
		int flag;
		QTextCharFormat style;
	};

	QTextCharFormat format(const QString& color, const QString& style="");

	std::map<std::string, QTextCharFormat> m_styles;
	std::vector<std::string> m_keywords;
	std::vector<std::string> m_operators;
	std::vector<std::string> m_braces;

	std::vector<Rule> m_rules;

	Rule m_tri_single;
	Rule m_tri_double;

	void init();

	bool match_multi_line(const QString& text,
						  const Rule &rule);
public:
	QtPythonSyntaxHighlighter(QObject* parent);
	QtPythonSyntaxHighlighter(QTextDocument *parent);

	virtual void highlightBlock(const QString& text) override;
};

#endif // QTPYTHONSYNTAXHIGHLIGHTER_H
