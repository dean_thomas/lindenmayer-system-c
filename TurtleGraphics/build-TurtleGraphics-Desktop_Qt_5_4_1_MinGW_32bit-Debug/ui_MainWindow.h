/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <TurtleGraphics/TurtleGraphicsWidget.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSave;
    QAction *actionGenerate;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    QGroupBox *groupBoxLSystem;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *labelSymbol;
    QLabel *labelReplacementRule;
    QLabel *labelAction;
    QComboBox *comboBoxSymbol;
    QComboBox *comboBox_3;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEditRule;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *labelAxiom;
    QLineEdit *lineEdit;
    TurtleGraphicsWidget *widget;
    QComboBox *comboBoxIterations;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(450, 405);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionGenerate = new QAction(MainWindow);
        actionGenerate->setObjectName(QStringLiteral("actionGenerate"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Vertical);
        groupBoxLSystem = new QGroupBox(splitter);
        groupBoxLSystem->setObjectName(QStringLiteral("groupBoxLSystem"));
        groupBoxLSystem->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_2 = new QVBoxLayout(groupBoxLSystem);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        frame = new QFrame(groupBoxLSystem);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(0, 65));
        frame->setMaximumSize(QSize(16777215, 65));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        labelSymbol = new QLabel(frame);
        labelSymbol->setObjectName(QStringLiteral("labelSymbol"));

        gridLayout->addWidget(labelSymbol, 0, 0, 1, 1);

        labelReplacementRule = new QLabel(frame);
        labelReplacementRule->setObjectName(QStringLiteral("labelReplacementRule"));

        gridLayout->addWidget(labelReplacementRule, 0, 1, 1, 1);

        labelAction = new QLabel(frame);
        labelAction->setObjectName(QStringLiteral("labelAction"));

        gridLayout->addWidget(labelAction, 0, 2, 1, 1);

        comboBoxSymbol = new QComboBox(frame);
        comboBoxSymbol->setObjectName(QStringLiteral("comboBoxSymbol"));

        gridLayout->addWidget(comboBoxSymbol, 1, 0, 1, 1);

        comboBox_3 = new QComboBox(frame);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));
        comboBox_3->setMinimumSize(QSize(150, 0));

        gridLayout->addWidget(comboBox_3, 1, 2, 1, 1);

        frame_2 = new QFrame(frame);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(frame_2);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(16, 16777215));

        horizontalLayout->addWidget(label);

        lineEditRule = new QLineEdit(frame_2);
        lineEditRule->setObjectName(QStringLiteral("lineEditRule"));

        horizontalLayout->addWidget(lineEditRule);


        gridLayout->addWidget(frame_2, 1, 1, 1, 1);


        verticalLayout_2->addWidget(frame);

        frame_3 = new QFrame(groupBoxLSystem);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(0, 50));
        frame_3->setMaximumSize(QSize(16777215, 50));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(171, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        labelAxiom = new QLabel(frame_3);
        labelAxiom->setObjectName(QStringLiteral("labelAxiom"));

        horizontalLayout_2->addWidget(labelAxiom);

        lineEdit = new QLineEdit(frame_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout_2->addWidget(frame_3);

        splitter->addWidget(groupBoxLSystem);
        widget = new TurtleGraphicsWidget(splitter);
        widget->setObjectName(QStringLiteral("widget"));
        splitter->addWidget(widget);

        verticalLayout->addWidget(splitter);

        comboBoxIterations = new QComboBox(centralWidget);
        comboBoxIterations->setObjectName(QStringLiteral("comboBoxIterations"));

        verticalLayout->addWidget(comboBoxIterations);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 450, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        mainToolBar->addAction(actionSave);
        mainToolBar->addAction(actionGenerate);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionSave->setText(QApplication::translate("MainWindow", "&Save...", 0));
        actionGenerate->setText(QApplication::translate("MainWindow", "Generate", 0));
        groupBoxLSystem->setTitle(QApplication::translate("MainWindow", "Lindenmayer system parameters", 0));
        labelSymbol->setText(QApplication::translate("MainWindow", "Symbol:", 0));
        labelReplacementRule->setText(QApplication::translate("MainWindow", "Replacement Rule:", 0));
        labelAction->setText(QApplication::translate("MainWindow", "Action:", 0));
        label->setText(QApplication::translate("MainWindow", "->", 0));
        labelAxiom->setText(QApplication::translate("MainWindow", "Axiom:", 0));
        comboBoxIterations->clear();
        comboBoxIterations->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "0", 0)
         << QApplication::translate("MainWindow", "1", 0)
         << QApplication::translate("MainWindow", "2", 0)
         << QApplication::translate("MainWindow", "3", 0)
         << QApplication::translate("MainWindow", "4", 0)
         << QApplication::translate("MainWindow", "5", 0)
         << QApplication::translate("MainWindow", "6", 0)
         << QApplication::translate("MainWindow", "7", 0)
         << QApplication::translate("MainWindow", "8", 0)
         << QApplication::translate("MainWindow", "9", 0)
         << QApplication::translate("MainWindow", "10", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
