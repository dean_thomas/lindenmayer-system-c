#ifndef QT_TURTLE_CURSOR_H
#define QT_TURTLE_CURSOR_H

#include <QGraphicsItem>
#include <QPen>
#include <QBrush>
#include <QRectF>
#include <QColor>
#include <QPainterPath>
#include <QStack>
#include <QVector2D>

//#include <iostream>

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;

#define PI 3.14159265
#define TO_RADS(x) x*(PI/180.0f)


class QtTurtleCursor : public QGraphicsItem
{
		friend std::ostream& operator<<(std::ostream&, const QtTurtleCursor&);
	private:
		//  Used to store cursor position and orientation
		struct StackEntry
		{
			QPointF pos;
			double angle;
		};
		using PositionStack = QStack<StackEntry>;

		const QColor COLOUR_PEN_UP = QColor(Qt::red);
		const QColor COLOUR_PEN_DOWN = QColor(Qt::green);

		QPainterPath m_outlinePath;
		QPen m_outlinePen;
		QBrush m_brush;

		float m_angle = 0.0f;
		bool m_penDown = true;

		PositionStack m_stack;

		QPointF m_startPos;
	public:
		const float RADIUS = 5.0f;

		QRectF boundingRect() const override;

		void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *) override;

		explicit QtTurtleCursor(const QPointF& pos);

		void turnClockwise(const float degrees);
		void turnAntiClockwise(const float degrees);

		void penUp();
		void penDown();
		void penToggle();

		void move(const float distance);
		void moveBack(const float distance);
		void moveForward(const float distance);

		void push();
		void pop();

		void reset();

		bool isPenDown() const { return m_penDown; }
		size_t getStackSize() const { return m_stack.size(); }
		float getCurrentAngle() const { return m_angle; }
		QPointF getCurrentPosition() const { return pos(); }
		QVector2D getVectorToOrigin() const { return QVector2D(pos()) - QVector2D(m_startPos); }
};

#endif // QT_TURTLE_CURSOR_H
