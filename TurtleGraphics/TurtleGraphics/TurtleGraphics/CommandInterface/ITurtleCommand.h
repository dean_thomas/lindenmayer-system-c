#ifndef ITURTLECOMMAND_H
#define ITURTLECOMMAND_H

#include <cassert>
#include <vector>
#include <string>
#include <sstream>
#include <memory>
#include "../QtTurtleCursor.h"

using std::vector;
using std::string;
using std::stringstream;

///
/// \brief  All turtle commands inherit from this
/// \since  14-08-2015
/// \author Dean
///
class ITurtleCommand
{
public:
	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const = 0;
	virtual std::string ToString() const = 0;
	virtual ~ITurtleCommand();
};

///
/// \brief  Represents a command that does
///         nothing when executed
/// \since  14-08-2015
/// \author Dean
///
class NullCommand : public ITurtleCommand
{
public:
	NullCommand() { }

	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const;
	virtual string ToString() const;
};

///
/// \brief  Represents a command that carries out
///         multiple commands upon execution.
/// \since  14-08-2015
/// \author Dean
///
class MacroCommand : public ITurtleCommand
{
private:
	using PtrCommand = std::unique_ptr<ITurtleCommand>;
	using CommandList = std::vector<PtrCommand>;
	CommandList m_commands;
public:
	MacroCommand(PtrCommand &&existingCommand, PtrCommand &&newCommand);
	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const;
	virtual string ToString() const;
	virtual ~MacroCommand();
};

///
/// \brief  Issues a command to rotate the turtle cursor
/// \since  14-08-2015
/// \author Dean
///
class RotateCommand : public ITurtleCommand
{
public:
	//  Not used at the moment
	enum class Action
	{
		CLOCKWISE,
		ANTI_CLOCKWISE
	};
private:
	QtTurtleCursor *m_turtle = nullptr;
	Action m_action = Action::CLOCKWISE;
	float m_degrees;
public:
	RotateCommand(QtTurtleCursor *turtle, const Action &action, const float &degrees)
		: m_turtle{turtle}, m_action{action}, m_degrees{degrees} {}

	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const;
	virtual string ToString() const;
	virtual ~RotateCommand();
};

///
/// \brief  Issues a command to move the turtle cursor
/// \since  14-08-2015
/// \author Dean
///
class MoveCommand : public ITurtleCommand
{
public:
	//  Not used at the moment
	enum class Action
	{
		FORWARD,
		BACK
	};
private:
	QtTurtleCursor *m_turtle = nullptr;
	float m_distance;
public:
	MoveCommand(QtTurtleCursor *turtle, const float &distance)
		: m_turtle{turtle}, m_distance{distance} {}

	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const;
	virtual string ToString() const;
	virtual ~MoveCommand();
};

///
/// \brief  Issues a command to store or retrieve a
///         turtle cursor position
/// \since  14-08-2015
/// \author Dean
///
class StackCommand : public ITurtleCommand
{
public:
	enum class Action
	{
		PUSH,
		POP
	};
private:
	QtTurtleCursor *m_turtle = nullptr;
	Action m_action = Action::PUSH;
public:
	StackCommand(QtTurtleCursor *turtle, const Action &action)
		: m_turtle{turtle}, m_action{action} {}

	virtual QVector<QLineF> Execute(const QVector<QLineF> &path) const;
	virtual string ToString() const;
	virtual ~StackCommand();
};


#endif // ITURTLECOMMAND_H
