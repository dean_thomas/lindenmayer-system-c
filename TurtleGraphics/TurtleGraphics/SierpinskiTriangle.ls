#   Comments start with a hash symbol
#
#   Created on 04-05-2016
#
#   The title will indicate what is display in the user interface
title:  Sierpinski triangle

#   The axiom represents the start state of the system
axiom:  A

#   Replacement rules are represented one line per rule (and are case sensitive)
A:  +B-A-B+
B:  -A+B+A-
