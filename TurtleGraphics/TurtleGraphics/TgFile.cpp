#include "TgFile.h"
#include "CommandInterface/CommandInterface.h"

#include <cassert>
#include <fstream>
#include <string>
#include <regex>
#include <iostream>

using namespace std;

TgFileReader::TgFileReader(CommandInterface* const commandInterface)
{
    assert(commandInterface != nullptr);
    m_commandInterface = commandInterface;

    m_commentRegex = R"(\s*#(.*))";
    m_operatorRegex = R"(\s*(.+)\s*:.*)";
    m_moveRegex = R"(.*\s*(FWD)\s*(\d*).*)";
    m_rotateRegex = R"(.*\s*(ROT)\s*([+-]?\d*(?:[\.]\d+)?).*)";
    m_stackRegex = R"(.*\s*(PUSH|POP).*)";
    m_penRegex = R"(\s*PEN\s*(UP|DOWN)?.*)";
}

bool TgFileReader::parseCommand(const char op, const std::string& buffer) const
{
    smatch regexCommandsMatches;

    if (regex_match(buffer, regexCommandsMatches, m_moveRegex))
    {
        assert(regexCommandsMatches.size() == 3);

        //  FWD etc...
        auto dir = regexCommandsMatches[1].str();

        //  10 etc...
        auto param = stof(regexCommandsMatches[2].str());

        //  e.g.    FWD 10
        auto newCommand = m_commandInterface->GenerateMoveCommand(param);
        auto description = string("Move forward ") + to_string(param) + string(" units.");

        m_commandInterface->AddCommand(op, description, newCommand);

        return true;
    }
    else if (regex_match(buffer, regexCommandsMatches, m_rotateRegex))
    {
        assert(regexCommandsMatches.size() == 3);

        //  ROT
        //auto dir = regexCommandsMatches[1].str();

        //  +10.0 etc...
        auto param = stof(regexCommandsMatches[2].str());

        //  e.g.    FWD 10
        auto newCommand = m_commandInterface->GenerateTurnCommand(param);
        auto description = string("Turn ") + to_string(param) + string(" degrees.");

        m_commandInterface->AddCommand(op, description, newCommand);

        return true;
    }
    else if (regex_match(buffer, regexCommandsMatches, m_stackRegex))
    {
        assert(regexCommandsMatches.size() == 2);

        //  PUSH or POP
        auto isPush = regexCommandsMatches[1].str() == "PUSH";

        //  e.g.    FWD 10
        auto newCommand = m_commandInterface->GenerateStackCommand(isPush);
        auto description = (isPush ? "Push to stack." : "Pop from stack");

        m_commandInterface->AddCommand(op, description, newCommand);

        return true;
    }
}

void TgFileReader::operator ()(const std::string& filename)
{
    ifstream inputFile(filename);
    assert(inputFile.is_open() && inputFile.good());


    m_commandInterface->Clear();

    while (!inputFile.eof())
    {
        string buffer;
        getline(inputFile, buffer);

        //cout << "Buffer: " << buffer << endl;

        smatch regexOperatorMatches;

        if (regex_match(buffer, regexOperatorMatches, m_commentRegex))
        {
            cout << "Comment: ";
            for (auto it = regexOperatorMatches.cbegin()+1; it != regexOperatorMatches.cend(); ++it)
            {
                cout << it->str();
            }
            cout << endl;
        }
        else if (regex_match(buffer, regexOperatorMatches, m_operatorRegex))
        {
            assert(regexOperatorMatches.size() == 2);
            auto op = regexOperatorMatches[1].str();

            cout << "Operator '" << op << "': ";

            bool result = parseCommand(op[0], buffer);

            cout << boolalpha << result << endl;
        }
        else
        {
            if (buffer != "") cerr << "Unrecognised text in file: " << buffer << endl;
        }
    }
    inputFile.close();
}
