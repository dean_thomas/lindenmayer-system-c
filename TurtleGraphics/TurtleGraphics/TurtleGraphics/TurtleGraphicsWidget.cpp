#include "TurtleGraphicsWidget.h"
#include "ui_TurtleGraphicsWidget.h"
#include "CommandInterface/ITurtleCommand.h"

#include <QGraphicsScene>
#include <iostream>
#include <QWheelEvent>


using namespace std;

void TurtleGraphicsWidget::execute_python_expression(const std::string& expression)
{
	assert(m_python_interface != nullptr);
	m_python_interface->execute_script(expression);
}

TurtleGraphicsWidget::TurtleGraphicsWidget(QWidget *parent) :
	QGraphicsView(parent),
	ui(new Ui::TurtleGraphicsWidget)
{
	ui->setupUi(this);

	m_python_interface = new PythonInterface(this);

	m_graphicsScene = new QGraphicsScene(rect(), this);
	this->setScene(m_graphicsScene);

	m_turtleCursor = new QtTurtleCursor(rect().center());
	m_graphicsScene->addItem(m_turtleCursor);

	m_turtlePath = new QtTurtlePath(rect().center());
	m_graphicsScene->addItem(m_turtlePath);

	//m_commandFactory = CommandFactory(&m_turtle);

	//m_turtle = QtTurtleCursor(QPointF(200.0f, 200.0f), 180.0f);
}

TurtleGraphicsWidget::~TurtleGraphicsWidget()
{
	delete ui;
}

void TurtleGraphicsWidget::wheelEvent(QWheelEvent* event)
{
	auto delta = event->delta();

	cout << "Wheel delta: " << delta << endl;

	if (delta < 0.0f)
	{
		m_scale -= 0.05f;
	}
	else if (delta > 0.0f)
	{
		m_scale += 0.05f;
	}

	resetMatrix();
	scale(m_scale, m_scale);
}

void TurtleGraphicsWidget::rotateCursorPos(const float degrees)
{
	m_turtleCursor->turnClockwise(degrees);

	//m_graphicsScene->update();
}

void TurtleGraphicsWidget::rotateCursorNeg(const float degrees)
{
	m_turtleCursor->turnAntiClockwise(degrees);

	//m_graphicsScene->update();
}

void TurtleGraphicsWidget::penDown()
{
	m_turtleCursor->penDown();

	//m_graphicsScene->update();
}

void TurtleGraphicsWidget::penUp()
{
	m_turtleCursor->penUp();

	//m_graphicsScene->update();
}

void TurtleGraphicsWidget::penToggle()
{
	m_turtleCursor->penToggle();

	//m_graphicsScene->update();
}

void TurtleGraphicsWidget::move(const float distance)
{
	m_turtleCursor->move(distance);

	if (m_turtleCursor->isPenDown())
	{
		m_turtlePath->lineTo(m_turtleCursor->pos());
	}
	else
	{
		m_turtlePath->moveTo(m_turtleCursor->pos());
	}
}

void TurtleGraphicsWidget::moveForward(const float distance)
{
	move(distance);
}

void TurtleGraphicsWidget::moveBack(const float distance)
{
	move(-distance);
}

void TurtleGraphicsWidget::push()
{
	m_turtleCursor->push();
}

void TurtleGraphicsWidget::pop()
{
	m_turtleCursor->pop();

	m_turtlePath->moveTo(m_turtleCursor->pos());
}

void TurtleGraphicsWidget::reset()
{
    m_turtlePath->reset();
	m_turtleCursor->reset();
}
