#include "QtTurtlePath.h"

QtTurtlePath::QtTurtlePath(const QPointF &pos)
	: m_startPos{ pos }
{
	QPainterPath newPath(pos);

	setPath(newPath);
}

void QtTurtlePath::moveTo(const QPointF& pos)
{
	auto currentPath = path();

	currentPath.moveTo(pos);

	setPath(currentPath);

	update();
}

void QtTurtlePath::lineTo(const QPointF& pos)
{
	auto currentPath = path();

	currentPath.lineTo(pos);

	setPath(currentPath);

	update();
}

void QtTurtlePath::reset()
{
	QPainterPath newPath(m_startPos);

	setPath(newPath);

	update();
}

float QtTurtlePath::getPathLength() const
{
	auto currentPath = path();

	return currentPath.length();
}
