#include "python_interface.h"
#include "TurtleGraphicsWidget.h"
#include <cassert>
#include <iostream>
#include <QDebug>

using namespace std;

PythonInterface* obj = nullptr;

PyObject* cursor_reset_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'reset' member function";

	return obj->cursor_reset(self, args);
}

PyObject* cursor_forward_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'forward' member function";

	return obj->cursor_forward(self, args);
}

PyObject* cursor_back_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'back' member function";

	return obj->cursor_back(self, args);
}

PyObject* cursor_move_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'move' member function";

	return obj->cursor_move(self, args);
}

PyObject* cursor_rot_pos_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'rotate pos' member function";

	return obj->cursor_rotate_pos(self, args);
}

PyObject* cursor_rot_neg_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'rotate neg' member function";

	return obj->cursor_rotate_neg(self, args);
}

PyObject* cursor_rot_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'rotate' member function";

	return obj->cursor_rotate(self, args);
}

PyObject* cursor_push_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'push' member function";

	return obj->push(self, args);
}

PyObject* cursor_pop_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'pop' member function";

	return obj->pop(self, args);
}

PyObject* cursor_pen_up_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'pen up' member function";

	return obj->pen_up(self, args);
}

PyObject* cursor_pen_down_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'pen down' member function";

	return obj->pen_down(self, args);
}

PyObject* cursor_pen_toggle_handler(PyObject *self, PyObject *args)
{
	assert(obj != nullptr);
	qDebug() << "Calling 'pen toggle' member function";

	return obj->pen_toggle(self, args);
}

static PyMethodDef methods[] = {
	//	Reset command
	{"reset", cursor_reset_handler, METH_NOARGS, "Resets the cursor to its default state."},

	//	Movement commands
	{"forward", cursor_forward_handler, METH_O, "Moves the cursor in the forward direction."},
	{"backward", cursor_back_handler, METH_O, "Moves the cursor in the backward direction."},
	{"move", cursor_move_handler, METH_O, "Moves the cursor in the either direction."},

	//	Turn commands
	{"turn_left", cursor_rot_neg_handler, METH_O, "Turns the cursor anticlockwise (in degrees)."},
	{"turn_right", cursor_rot_pos_handler, METH_O, "Turns the cursor clockwise (in degrees)."},
	{"turn", cursor_rot_handler, METH_O, "Turns the cursor either direction (in degrees)."},

	//	Stack commands
	{"push", cursor_push_handler, METH_NOARGS, "Pushes the current cursor position onto the stack."},
	{"pop", cursor_pop_handler, METH_NOARGS, "Pops the top cursor position off the stack."},

	//	Pen commands
	{"pen_up", cursor_pen_up_handler, METH_NOARGS, "Lifts the pen off the 'paper'."},
	{"pen_down", cursor_pen_down_handler, METH_NOARGS, "Places the pen in contact with the 'paper'."},
	{"pen_toggle", cursor_pen_toggle_handler, METH_NOARGS, "Toggles the status of the pen."},

	{NULL, NULL, 0, NULL}
};

static PyModuleDef module = {
	PyModuleDef_HEAD_INIT, "embedded", NULL, -1, methods,
	NULL, NULL, NULL, NULL
};

static PyObject* PyInit_emb(void)
{
	return PyModule_Create(&module);
}

PyObject* PythonInterface::cursor_reset(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Reseting cursor to deafults." << endl;

	//	Move the cursor
	m_widget->reset();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_forward(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Moving cursor forward" << arg0 << "units." << endl;

	//	Move the cursor
	m_widget->moveForward(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_back(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Moving cursor back" << arg0 << "units." << endl;

	//	Move the cursor
	m_widget->moveBack(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_move(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Moving cursor " << arg0 << "units." << endl;

	//	Move the cursor
	m_widget->move(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_rotate_pos(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Rotating cursor " << arg0 << "degrees." << endl;

	//	Move the cursor
	m_widget->rotateCursorPos(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_rotate_neg(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Rotating cursor" << -arg0 << "degrees." << endl;

	//	Move the cursor
	m_widget->rotateCursorNeg(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::cursor_rotate(PyObject *, PyObject *args)
{
	assert(m_widget != nullptr);

	//	Test that a valid numeric value has been passed
	auto parse_result = PyFloat_Check(args);
	assert(parse_result);
	if (!parse_result) return nullptr;

	//	Ok to go ahead and do the parse
	auto arg0 = static_cast<float>(PyFloat_AsDouble(args));
	assert(!PyErr_Occurred());

	qDebug() << "Rotating cursor" << arg0 << "degrees." << endl;

	//	Move the cursor
	m_widget->rotate(arg0);

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::push(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Pushing cursor position onto stack." << endl;

	//	Move the cursor
	m_widget->push();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::pop(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Popping cursor position from stack." << endl;

	//	Move the cursor
	m_widget->pop();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::pen_up(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Putting pen in 'up' position." << endl;

	//	Move the cursor
	m_widget->penUp();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::pen_down(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Putting pen in 'down' position." << endl;

	//	Move the cursor
	m_widget->penDown();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

PyObject* PythonInterface::pen_toggle(PyObject *, PyObject *)
{
	assert(m_widget != nullptr);

	qDebug() << "Toggling pen position." << endl;

	//	Move the cursor
	m_widget->penToggle();

	//	There is no result but we must return an object
	//	to signify a success
	return Py_None;
}

//void penUp();
//void penDown();
//void penToggle();



//void push();
//void pop();

//void reset();

PythonInterface::PythonInterface(TurtleGraphicsWidget* target)
	: m_widget{ target }
{
	qDebug() << "Setting up Python interface";
	obj = this;

	PyImport_AppendInittab("embedded", &PyInit_emb);

	Py_Initialize();

	//	Nice fix from here
	//	https://stackoverflow.com/questions/24492327/python-embedding-in-c-importerror-no-module-named-pyfunction/35582046#35582046
	//	to make sure the Python interpreter is able to find python code in the directory of the executable
	//
	PyRun_SimpleString("import sys");
	PyRun_SimpleString("sys.path.append(\".\")");

	PyRun_SimpleString("from embedded import *");
}

void PythonInterface::execute_script(const std::string& script)
{
	qDebug() << __func__;
	PyRun_SimpleString(script.c_str());
}


