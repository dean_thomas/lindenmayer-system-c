#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <map>

using std::vector;
using std::string;
using std::stringstream;
using std::map;

class LSystem
{
private:
	using RuleLookupTable = map<char, string>;
	
	string m_title;
	string m_axiom;
	RuleLookupTable m_rules;

	bool validateSystem() const;
	string lookupRule(const char &character) const;
public:
	LSystem(const string &title, const string &axiom);
	LSystem(const string &title, const string &axiom, const map<char, string> &rules);

	vector<char> getConstants() const;

	void AddRule(const char &character, const string &replacementRule);

	~LSystem();

	string Generate(const long &iterationCount) const;

	string ToString() const;
};

