# README #

This is an ongoing project used to test various concepts I am interested in:

* Generation of Lindenmayer systems (https://en.wikipedia.org/wiki/L-system) in C++.
* Using the generated Lindenmayer system code to draw fractal patterns using Turtle graphics (https://en.wikipedia.org/wiki/Turtle_graphics).
* Implementing a turtle graphics interface in Qt5.
* Allowing the user to modify behaviour at runtime by providing a basic Python scripting interface embedded in the main C++ code.
* Experimenting with design patterns (i.e using Pure Abstract classes to simulate Java interfaces).
* Experimenting with functional programming techniques in C++ such as std::bind and function prototype objects.