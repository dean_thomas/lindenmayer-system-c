#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <algorithm>

using std::vector;
using std::string;
using std::stringstream;
using std::map;
using std::transform;

#define DICTIONARY "[](){}<>ABCDEFGHIJKLMNOPOQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"

class LSystem
{
private:
	using RuleLookupTable = map<char, string>;
	
	string m_title;
	string m_axiom;
	RuleLookupTable m_rules;

	bool validateSystem() const;

public:
    LSystem();
	LSystem(const string &title, const string &axiom);
	LSystem(const string &title, const string &axiom, const map<char, string> &rules);

    vector<char> GetVariables() const;
    vector<char> GetConstants() const;
    vector<char> GetAvailableSymbols() const;

    string LookupRule(const char &character) const;

    void SetAxiom(const string &axiom);
    std::string GetAxiom() const { return m_axiom; }
    void SetTitle(const std::string& title);
    std::string GetTitle() const { return m_title; }

	void AddRule(const char &character, const string &replacementRule);

	~LSystem();

    string Generate(const unsigned long &iterationCount) const;

    bool IsValid() const { return validateSystem(); }
	string ToString() const;
};

