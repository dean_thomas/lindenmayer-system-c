#ifndef TGFILE_H
#define TGFILE_H

#include <string>
#include <regex>

class CommandInterface;

class TgFileReader
{
    //  Command <cmd, <params...>>
    using Command = std::pair<std::string, std::vector<std::string>>;
private:
    bool parseCommand(const char op, const std::string& buffer) const;

    CommandInterface* m_commandInterface = nullptr;

    std::regex m_commentRegex;
    std::regex m_operatorRegex;
    std::regex m_moveRegex;
    std::regex m_rotateRegex;
    std::regex m_stackRegex;
    std::regex m_penRegex;
public:
    TgFileReader(CommandInterface* const commandInterface);
    void operator ()(const std::string& filename);
};

#endif // TGFILE_H
