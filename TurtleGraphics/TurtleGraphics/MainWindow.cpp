#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "RuleDialog.h"
#include "LsFile.h"
#include "TgFile.h"
#include "commands_dialog.h"

#include <iostream>
#include <thread>
#include <chrono>
#include <QTime>
#include <QFileDialog>
#include <QDebug>

using namespace std;



void MainWindow::initPythagorasTree()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Pythagoras Tree", "0", { { '1', "11" }, { '0', "1[0]0" } });

	//  Set up the drawing interface
	//m_commandInterface = CommandInterface;

	m_commandInterface.AddCommand('0', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 2.5f));
	m_commandInterface.AddCommand('1', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));
	m_commandInterface.AddCommand('[', "", [&]()
	{
		ui->widget->push();
		ui->widget->rotateCursorPos(45.0f);
	});
	m_commandInterface.AddCommand(']', "", [&]()
	{
		ui->widget->pop();
		ui->widget->rotateCursorNeg(45.0f);
	});

	/*
								  std::bind(&TurtleGraphicsWidget::push, ui->widget);
	ui->widget->AddCommand('[', RotateCommand::Action::CLOCKWISE, 45.0f);
	ui->widget->AddCommand(']', StackCommand::Action::POP);
	ui->widget->AddCommand(']', RotateCommand::Action::ANTI_CLOCKWISE, 45.0f);
	*/
}

void MainWindow::initKochCurve()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Koch Curve", "F", { { 'F', "F+F-F-F+F" } });

	auto forward5 = [&]()
	{
		ui->widget->moveForward(15.0f);
	};

	auto turnPos60 = [&]()
	{
		ui->widget->rotateCursorPos(60.0f);
	};

	auto turnNeg60 = [&]()
	{
		ui->widget->rotateCursorNeg(60.0f);
	};

	m_commandInterface.AddCommand('F',  "", forward5);
	m_commandInterface.AddCommand('+',  "", turnPos60);
	m_commandInterface.AddCommand('-',  "", turnNeg60);

}

void MainWindow::initSierpinskiTriangle()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Sierpinski Triangle", "A", { { 'A', "+B-A-B+" }, { 'B', "-A+B+A-" } });

	//  Set up the drawing interface
	m_commandInterface.AddCommand('A', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));
	m_commandInterface.AddCommand('B', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));
	m_commandInterface.AddCommand('+', "", std::bind(&TurtleGraphicsWidget::rotateCursorPos, ui->widget, 60.0f));
	m_commandInterface.AddCommand('-', "", std::bind(&TurtleGraphicsWidget::rotateCursorNeg, ui->widget, 60.0f));
}

void MainWindow::initDragonCurve()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Dragon Curve", "FX", { { 'X', "X+YF+" }, { 'Y', "-FX-Y" } });

	//  Set up the drawing interface
	m_commandInterface.AddCommand('F', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));
	m_commandInterface.AddCommand('X', "", [&](){});
	m_commandInterface.AddCommand('Y', "", [&](){});
	m_commandInterface.AddCommand('+', "", std::bind(&TurtleGraphicsWidget::rotateCursorPos, ui->widget, 90.0f));
	m_commandInterface.AddCommand('-', "", std::bind(&TurtleGraphicsWidget::rotateCursorNeg, ui->widget, 90.0f));
}

void MainWindow::initFractalPlant()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Fractal Plant", "X", { { 'X', "F-[[X]+X]+F[+FX]-X" }, { 'F', "FF" } });

	//  Set up the drawing interface
	m_commandInterface.AddCommand('F', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));
	m_commandInterface.AddCommand('X', "", [&](){});
	m_commandInterface.AddCommand('+', "", std::bind(&TurtleGraphicsWidget::rotateCursorPos, ui->widget, 25.0f));
	m_commandInterface.AddCommand('-', "", std::bind(&TurtleGraphicsWidget::rotateCursorNeg, ui->widget, 25.0f));
	m_commandInterface.AddCommand('[', "", std::bind(&TurtleGraphicsWidget::push, ui->widget));
	m_commandInterface.AddCommand(']', "", std::bind(&TurtleGraphicsWidget::pop, ui->widget));
}

void MainWindow::initBoxFractal()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Box Fractal", "F-F-F-F", { { 'F', "F-F+F+F-F" } });

	//  Set up the drawing interface
	m_commandInterface.AddCommand('+', "", std::bind(&TurtleGraphicsWidget::rotateCursorPos, ui->widget, 90.0f));
	m_commandInterface.AddCommand('-', "", std::bind(&TurtleGraphicsWidget::rotateCursorNeg, ui->widget, 90.0f));
	m_commandInterface.AddCommand('F', "", std::bind(&TurtleGraphicsWidget::moveForward, ui->widget, 5.0f));

}

void MainWindow::initSegmentCurve()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("32-Segment Curve Fractal", "F+F+F+F", { { 'F', "-F+F-F-F+F+FF-F+F+FF+F-F-FF+FF-FF+F+F-FF-F-F+FF-F-F+F+F-F+" } });
/*
	//  Set up the drawing interface
	ui->widget->ClearCommands();
	ui->widget->AddCommand('+', RotateCommand::Action::CLOCKWISE, 90.0f);
	ui->widget->AddCommand('-', RotateCommand::Action::ANTI_CLOCKWISE, 90.0f);
	ui->widget->AddCommand('F', MoveCommand::Action::FORWARD, 3.0f);
	*/
}

void MainWindow::initHilbertCurve()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Hilbert Curve", "L", { { 'L', "+RF-LFL-FR+" }, {'R', "-LF+RFR+FL-" }});
/*
	//  Set up the drawing interface
	ui->widget->ClearCommands();
	ui->widget->AddCommand('+', RotateCommand::Action::CLOCKWISE, 90.0f);
	ui->widget->AddCommand('-', RotateCommand::Action::ANTI_CLOCKWISE, 90.0f);
	ui->widget->AddCommand('L');
	ui->widget->AddCommand('R');
	ui->widget->AddCommand('F', MoveCommand::Action::FORWARD, 4.0f);
	*/
}

void MainWindow::initPeanoGosperCurve()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Peano-Gosper Curve", "FX", { { 'X', "X+YF++YF-FX--FXFX-YF+" }, {'Y', "-FX+YFYF++YF+FX--FX-Y" }});
/*
	//  Set up the drawing interface
	ui->widget->ClearCommands();
	ui->widget->AddCommand('+', RotateCommand::Action::CLOCKWISE, 60.0f);
	ui->widget->AddCommand('-', RotateCommand::Action::ANTI_CLOCKWISE, 60.0f);
	ui->widget->AddCommand('X');
	ui->widget->AddCommand('Y');
	ui->widget->AddCommand('F', MoveCommand::Action::FORWARD, 5.0f);
	*/
}

void MainWindow::initPeanoCurve()
{
	/*
	//  Set up the L-System generator
	m_lSystem = LSystem("Peano Curve", "F", { { 'F', "F+F-F-F-F+F+F+F-F" }});

	//  Set up the drawing interface
	ui->widget->ClearCommands();
	ui->widget->AddCommand('+', RotateCommand::Action::CLOCKWISE, 90.0f);
	ui->widget->AddCommand('-', RotateCommand::Action::ANTI_CLOCKWISE, 90.0f);
	ui->widget->AddCommand('F', MoveCommand::Action::FORWARD, 5.0f);
	*/
}

void MainWindow::initQuadraticKochIsland()
{
	//  Set up the L-System generator
	m_lSystem = LSystem("Quadratic Koch island", "F+F+F+F", { { 'F', "F-F+F+FFF-F-F+F" }});
/*
	//  Set up the drawing interface
	ui->widget->ClearCommands();
	ui->widget->AddCommand('+', RotateCommand::Action::CLOCKWISE, 90.0f);
	ui->widget->AddCommand('-', RotateCommand::Action::ANTI_CLOCKWISE, 90.0f);
	ui->widget->AddCommand('F', MoveCommand::Action::FORWARD, 5.0f);
	*/
}

bool MainWindow::loadFile()
{

	return true;
}

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	//	Setup code highlighter
	m_highlighter = new QtPythonSyntaxHighlighter(ui->textEditPythonCode->document());

	//ui->dockWidgetLSystem->setHidden(true);

	connect(ui->comboBoxSymbol, &QComboBox::currentTextChanged,
			[&](const QString& text)
	{
		if (text == ADD_SYMBOL_TEXT)
		{
			//  Allow the user to define a new symbol and rule
			RuleDialog dialog(m_lSystem, this);
			if (dialog.exec() == QDialog::Accepted)
			{
				//  Add the rule to the system
				m_lSystem.AddRule(dialog.GetSelectedCharacter(), dialog.GetReplacementString());
				updateRules();

				//  Select the new symbol/rule
				unsigned long index = ui->comboBoxSymbol->findText(QString(dialog.GetSelectedCharacter()));
				ui->comboBoxSymbol->setCurrentIndex(index);

				//  Display the rule
				ui->lineEditRule->setText(QString::fromStdString(
											  m_lSystem.LookupRule(ui->comboBoxSymbol->currentText().toStdString()[0])));
			}
		}
		else if (text.length() == 1)
		{
			//  Display an existing rule
			ui->lineEditRule->setText(QString::fromStdString(m_lSystem.LookupRule(text.toStdString()[0])));
		}
	});

	loadFile();

	updateRules();

    m_commandInterface.SetTargetWidget(ui->widget);
}

MainWindow::~MainWindow()
{
	delete m_highlighter;
	delete ui;
}

void MainWindow::updateRules()
{
	ui->comboBoxSymbol->clear();
	//ui->comboBox_2->clear();
	//ui->comboBox_3->clear();

	//  Variable list
	vector<char> variables = m_lSystem.GetVariables();

	if (variables.size() == 0)
	{
		ui->comboBoxSymbol->addItem(EMPTY_TEXT);
	}
	else
	{
		for (vector<char>::const_iterator it = variables.begin();
			 it != variables.end(); ++it)
		{
			ui->comboBoxSymbol->addItem(QString(*it));
		}
	}
	//  Item to allow the user to define a new rule
	ui->comboBoxSymbol->addItem(ADD_SYMBOL_TEXT);
}

void MainWindow::on_actionSave_triggered()
{
	//ui->widget->SaveToFile("");
}

void MainWindow::on_actionGenerate_triggered()
{
	unsigned long iterations = ui->comboBoxIterations->currentText().toULong();
	string commandString = m_lSystem.Generate(iterations);
	//m_lSystem.SetAxiom(ui->lineEdit->text().toStdString());
	//cout << m_lSystem.ToString() << "\n";
	//cout << commandString << "\n";
	//fflush(stdout);

   // ui->widget->SetCommandString(commandString);
}

void MainWindow::on_comboBoxIterations_currentIndexChanged(int)
{
	on_actionGenerate_triggered();
}

void MainWindow::on_comboBoxPreset_currentIndexChanged(int index)
{
	switch (index)
	{
	case 0:
		initPythagorasTree();
		break;
	case 1:
		initKochCurve();
		break;
	case 2:
		initSierpinskiTriangle();
		break;
	case 3:
		initDragonCurve();
		break;
	case 4:
		initFractalPlant();
		break;
	case 5:
		initBoxFractal();
		break;
	case 6:
		initSegmentCurve();
		break;
	case 7:
		initHilbertCurve();
		break;
	case 8:
		initPeanoGosperCurve();
		break;
	case 9:
		initPeanoCurve();
		break;
	case 10:
		initQuadraticKochIsland();
		break;
	}
	ui->widget->repaint();
}

void MainWindow::on_pushButtonRoatetNeg_clicked()
{
	ui->widget->rotateCursorNeg(30.0f);
}

void MainWindow::on_pushButtonRotatePos_clicked()
{
	ui->widget->rotateCursorPos(30.0f);
}

void MainWindow::on_pushButtonPenDown_clicked()
{
	ui->widget->penDown();
}

void MainWindow::on_pushButtonPenUp_clicked()
{
	ui->widget->penUp();
}

void MainWindow::on_pushButtonBack_clicked()
{
	ui->widget->moveBack(5.0f);
}

void MainWindow::on_pushButtonForward_clicked()
{
	ui->widget->moveForward(5.0f);
}

void MainWindow::on_pushButtonToggle_clicked()
{
	ui->widget->penToggle();
}

void MainWindow::on_pushButtonStackPop_clicked()
{
	ui->widget->pop();
}

void MainWindow::on_pushButtonPush_clicked()
{
	ui->widget->push();
}

void MainWindow::pause(const size_t milliseconds)
{
	QTime dieTime= QTime::currentTime().addMSecs(milliseconds);
	while (QTime::currentTime() < dieTime)
	{
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
	}
	//std::chrono::duration<double, std::milli> duration(milliseconds);
	//std::this_thread::sleep_for(duration);
}

void MainWindow::on_pushButtonExecute_clicked()
{
	//initPythagorasTree();
	//initKochCurve();
	//initSierpinskiTriangle();
	//initDragonCurve();
	//initFractalPlant();
    //initBoxFractal();

    auto output = m_lSystem.Generate(ui->comboBoxIterations->currentIndex());
	auto commands = m_commandInterface.parseString(output);

	ui->widget->reset();
	for (auto& f : commands)
	{
		f();
		pause(5);
	}
}

void MainWindow::on_actionOpen_triggered()
{
    auto filenameLS = QFileDialog::getOpenFileName(this, "Open file", "", "Lindenmayer System (*.ls)");
    auto filenameTG = QFileDialog::getOpenFileName(this, "Open file", "", "Turtle graphics (*.tg)");

    if ((filenameLS != "") && (filenameTG != ""))
    {
        try
        {
            LsFileReader fileReaderLS;
            auto resultLS = fileReaderLS(filenameLS.toStdString());
            cout << resultLS.ToString() << endl;
            m_lSystem = resultLS;

            TgFileReader fileReaderTG(&m_commandInterface);
            fileReaderTG(filenameTG.toStdString());
            cout << m_commandInterface << endl;

        }
        catch (std::exception& ex)
        {
            cerr << ex.what() << endl;
        }

    }
}

void MainWindow::on_pushButton_clicked()
{
	qDebug() << "Pressed button";

	ui->widget->execute_python_expression(ui->textEditPythonCode->toPlainText().toStdString());
}

void MainWindow::on_pushButton_2_clicked()
{
	QWidget* commands_dialog = new CommandsDialog(this);
	commands_dialog->show();
}
