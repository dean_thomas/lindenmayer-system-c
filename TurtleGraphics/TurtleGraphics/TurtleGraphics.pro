#-------------------------------------------------
#
# Project created by QtCreator 2015-08-12T16:18:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = TurtleGraphics
TEMPLATE = app

INCLUDEPATH += /usr/include/python3.5m

LIBS += -L/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu
LIBS += -lpython3.5m

QMAKE_CXXFLAGS += -Xlinker -export-dynamic -Wl,-O1 -Wl,-Bsymbolic-functions


SOURCES += main.cpp\
        MainWindow.cpp \
    TurtleGraphics/TurtleGraphicsWidget.cpp \
    TurtleGraphics/CommandInterface/ITurtleCommand.cpp \
    LSystem/LSystem.cpp \
    TurtleGraphics/QtTurtleCursor.cpp \
    TurtleGraphics/CommandInterface/CommandFactory.cpp \
    RuleDialog.cpp \
    TurtleGraphics/QtTurtlePath.cpp \
    CommandInterface/CommandInterface.cpp \
    LsFile.cpp \
    TgFile.cpp \
    TurtleGraphics/python_interface.cpp \
    commands_dialog.cpp \
    TurtleGraphics/qt_python_syntax_highlighter.cpp

HEADERS  += MainWindow.h \
    TurtleGraphics/TurtleGraphicsWidget.h \
    TurtleGraphics/CommandInterface/ITurtleCommand.h \
    LSystem/LSystem.h \
    TurtleGraphics/QtTurtleCursor.h \
    TurtleGraphics/CommandInterface/CommandFactory.h \
    RuleDialog.h \
    TurtleGraphics/QtTurtlePath.h \
    CommandInterface/CommandInterface.h \
    LsFile.h \
    TgFile.h \
    TurtleGraphics/python_interface.h \
    commands_dialog.h \
    TurtleGraphics/qt_python_syntax_highlighter.h

FORMS    += MainWindow.ui \
    TurtleGraphics\TurtleGraphicsWidget.ui \
    RuleDialog.ui \
    commands_dialog.ui

DISTFILES += \
    KochCurve.ls \
    SierpinskiTriangle.ls \
    DragonCurve.ls \
    FractalPlant.ls \
    PythagorasTree.ls \
    DragonCurve.tg \
    FractalPlant.tg \
    SierpinskiTriangle.tg \
    KochCurve.tg
