#ifndef COMMANDFACTORY_H
#define COMMANDFACTORY_H

#include <memory>
#include "ITurtleCommand.h"

class QtTurtleCursor;

using std::unique_ptr;

class CommandFactory
{
    using PtrCommand = unique_ptr<ITurtleCommand>;
private:
    QtTurtleCursor *m_turtleCursor = nullptr;
public:
    CommandFactory();
    CommandFactory(QtTurtleCursor *cursor);
    ~CommandFactory();

    PtrCommand NewStackCommand(const StackCommand::Action &action);
    PtrCommand NewMoveCommand(const MoveCommand::Action &action, const float &distance);
    PtrCommand NewRotateCommand(const RotateCommand::Action &action, const float &degrees);
    PtrCommand NewMacroCommand(PtrCommand &&existingCommand, PtrCommand &&newCommand);
    PtrCommand NewNullCommand();
};

#endif // COMMANDFACTORY_H
