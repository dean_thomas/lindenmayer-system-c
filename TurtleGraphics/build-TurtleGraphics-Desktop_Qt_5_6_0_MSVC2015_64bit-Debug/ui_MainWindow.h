/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <TurtleGraphics/TurtleGraphicsWidget.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSave;
    QAction *actionGenerate;
    QAction *actionOpen;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    TurtleGraphicsWidget *widget;
    QFrame *frame_4;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *comboBoxPreset;
    QComboBox *comboBoxIterations;
    QPushButton *pushButtonExecute;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dockWidgetLSystem;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBoxLSystem;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEditRule;
    QLabel *labelAction;
    QLabel *labelSymbol;
    QLabel *labelReplacementRule;
    QComboBox *comboBoxSymbol;
    QComboBox *comboBox_3;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *labelAxiom;
    QLineEdit *lineEdit;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButtonRoatetNeg;
    QPushButton *pushButtonRotatePos;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButtonBack;
    QPushButton *pushButtonForward;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButtonPenDown;
    QPushButton *pushButtonPenUp;
    QPushButton *pushButtonToggle;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButtonStackPop;
    QPushButton *pushButtonPush;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(765, 405);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionGenerate = new QAction(MainWindow);
        actionGenerate->setObjectName(QStringLiteral("actionGenerate"));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Vertical);
        widget = new TurtleGraphicsWidget(splitter);
        widget->setObjectName(QStringLiteral("widget"));
        splitter->addWidget(widget);

        verticalLayout->addWidget(splitter);

        frame_4 = new QFrame(centralWidget);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        comboBoxPreset = new QComboBox(frame_4);
        comboBoxPreset->setObjectName(QStringLiteral("comboBoxPreset"));

        horizontalLayout_3->addWidget(comboBoxPreset);

        comboBoxIterations = new QComboBox(frame_4);
        comboBoxIterations->setObjectName(QStringLiteral("comboBoxIterations"));

        horizontalLayout_3->addWidget(comboBoxIterations);

        pushButtonExecute = new QPushButton(frame_4);
        pushButtonExecute->setObjectName(QStringLiteral("pushButtonExecute"));

        horizontalLayout_3->addWidget(pushButtonExecute);


        verticalLayout->addWidget(frame_4);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 765, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        dockWidgetLSystem = new QDockWidget(MainWindow);
        dockWidgetLSystem->setObjectName(QStringLiteral("dockWidgetLSystem"));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_3 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBoxLSystem = new QGroupBox(dockWidgetContents);
        groupBoxLSystem->setObjectName(QStringLiteral("groupBoxLSystem"));
        groupBoxLSystem->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_2 = new QVBoxLayout(groupBoxLSystem);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        frame = new QFrame(groupBoxLSystem);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setMinimumSize(QSize(0, 110));
        frame->setMaximumSize(QSize(16777215, 110));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        frame_2 = new QFrame(frame);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(frame_2);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(16, 16777215));

        horizontalLayout->addWidget(label);

        lineEditRule = new QLineEdit(frame_2);
        lineEditRule->setObjectName(QStringLiteral("lineEditRule"));

        horizontalLayout->addWidget(lineEditRule);


        gridLayout->addWidget(frame_2, 1, 1, 1, 1);

        labelAction = new QLabel(frame);
        labelAction->setObjectName(QStringLiteral("labelAction"));

        gridLayout->addWidget(labelAction, 2, 0, 1, 1);

        labelSymbol = new QLabel(frame);
        labelSymbol->setObjectName(QStringLiteral("labelSymbol"));

        gridLayout->addWidget(labelSymbol, 0, 0, 1, 1);

        labelReplacementRule = new QLabel(frame);
        labelReplacementRule->setObjectName(QStringLiteral("labelReplacementRule"));

        gridLayout->addWidget(labelReplacementRule, 0, 1, 1, 1);

        comboBoxSymbol = new QComboBox(frame);
        comboBoxSymbol->setObjectName(QStringLiteral("comboBoxSymbol"));

        gridLayout->addWidget(comboBoxSymbol, 1, 0, 1, 1);

        comboBox_3 = new QComboBox(frame);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));
        comboBox_3->setMinimumSize(QSize(150, 0));

        gridLayout->addWidget(comboBox_3, 3, 0, 1, 2);


        verticalLayout_2->addWidget(frame);

        frame_3 = new QFrame(groupBoxLSystem);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMinimumSize(QSize(0, 50));
        frame_3->setMaximumSize(QSize(16777215, 50));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(171, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        labelAxiom = new QLabel(frame_3);
        labelAxiom->setObjectName(QStringLiteral("labelAxiom"));

        horizontalLayout_2->addWidget(labelAxiom);

        lineEdit = new QLineEdit(frame_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout_2->addWidget(frame_3);


        verticalLayout_3->addWidget(groupBoxLSystem);

        dockWidgetLSystem->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockWidgetLSystem);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        dockWidgetContents_2 = new QWidget();
        dockWidgetContents_2->setObjectName(QStringLiteral("dockWidgetContents_2"));
        horizontalLayout_4 = new QHBoxLayout(dockWidgetContents_2);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        pushButtonRoatetNeg = new QPushButton(dockWidgetContents_2);
        pushButtonRoatetNeg->setObjectName(QStringLiteral("pushButtonRoatetNeg"));

        horizontalLayout_4->addWidget(pushButtonRoatetNeg);

        pushButtonRotatePos = new QPushButton(dockWidgetContents_2);
        pushButtonRotatePos->setObjectName(QStringLiteral("pushButtonRotatePos"));

        horizontalLayout_4->addWidget(pushButtonRotatePos);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        pushButtonBack = new QPushButton(dockWidgetContents_2);
        pushButtonBack->setObjectName(QStringLiteral("pushButtonBack"));

        horizontalLayout_4->addWidget(pushButtonBack);

        pushButtonForward = new QPushButton(dockWidgetContents_2);
        pushButtonForward->setObjectName(QStringLiteral("pushButtonForward"));

        horizontalLayout_4->addWidget(pushButtonForward);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        pushButtonPenDown = new QPushButton(dockWidgetContents_2);
        pushButtonPenDown->setObjectName(QStringLiteral("pushButtonPenDown"));

        horizontalLayout_4->addWidget(pushButtonPenDown);

        pushButtonPenUp = new QPushButton(dockWidgetContents_2);
        pushButtonPenUp->setObjectName(QStringLiteral("pushButtonPenUp"));

        horizontalLayout_4->addWidget(pushButtonPenUp);

        pushButtonToggle = new QPushButton(dockWidgetContents_2);
        pushButtonToggle->setObjectName(QStringLiteral("pushButtonToggle"));

        horizontalLayout_4->addWidget(pushButtonToggle);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        pushButtonStackPop = new QPushButton(dockWidgetContents_2);
        pushButtonStackPop->setObjectName(QStringLiteral("pushButtonStackPop"));

        horizontalLayout_4->addWidget(pushButtonStackPop);

        pushButtonPush = new QPushButton(dockWidgetContents_2);
        pushButtonPush->setObjectName(QStringLiteral("pushButtonPush"));

        horizontalLayout_4->addWidget(pushButtonPush);

        horizontalSpacer_2 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        dockWidget->setWidget(dockWidgetContents_2);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(8), dockWidget);

        mainToolBar->addAction(actionOpen);
        mainToolBar->addAction(actionSave);
        mainToolBar->addAction(actionGenerate);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionSave->setText(QApplication::translate("MainWindow", "&Save...", 0));
        actionGenerate->setText(QApplication::translate("MainWindow", "Generate", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "&Open...", 0));
        comboBoxPreset->clear();
        comboBoxPreset->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "PythagorasTree", 0)
         << QApplication::translate("MainWindow", "KochCurve", 0)
         << QApplication::translate("MainWindow", "SierpinskiTriangle", 0)
         << QApplication::translate("MainWindow", "DragonCurve", 0)
         << QApplication::translate("MainWindow", "FractalPlant", 0)
         << QApplication::translate("MainWindow", "BoxFractal", 0)
         << QApplication::translate("MainWindow", "SegmentCurve", 0)
         << QApplication::translate("MainWindow", "HilbertCurve  ", 0)
         << QApplication::translate("MainWindow", "PeanoGosperCurve", 0)
         << QApplication::translate("MainWindow", "PeanoCurve  ", 0)
         << QApplication::translate("MainWindow", "QuadraticKochIsland", 0)
        );
        comboBoxIterations->clear();
        comboBoxIterations->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "0", 0)
         << QApplication::translate("MainWindow", "1", 0)
         << QApplication::translate("MainWindow", "2", 0)
         << QApplication::translate("MainWindow", "3", 0)
         << QApplication::translate("MainWindow", "4", 0)
         << QApplication::translate("MainWindow", "5", 0)
         << QApplication::translate("MainWindow", "6", 0)
         << QApplication::translate("MainWindow", "7", 0)
         << QApplication::translate("MainWindow", "8", 0)
         << QApplication::translate("MainWindow", "9", 0)
         << QApplication::translate("MainWindow", "10", 0)
         << QApplication::translate("MainWindow", "11", 0)
         << QApplication::translate("MainWindow", "12", 0)
         << QApplication::translate("MainWindow", "13", 0)
         << QApplication::translate("MainWindow", "14", 0)
         << QApplication::translate("MainWindow", "15", 0)
         << QApplication::translate("MainWindow", "16", 0)
         << QApplication::translate("MainWindow", "17", 0)
         << QApplication::translate("MainWindow", "18", 0)
         << QApplication::translate("MainWindow", "19", 0)
         << QApplication::translate("MainWindow", "20", 0)
        );
        pushButtonExecute->setText(QApplication::translate("MainWindow", "Execute", 0));
        groupBoxLSystem->setTitle(QApplication::translate("MainWindow", "Lindenmayer system parameters", 0));
        label->setText(QApplication::translate("MainWindow", "->", 0));
        labelAction->setText(QApplication::translate("MainWindow", "Action:", 0));
        labelSymbol->setText(QApplication::translate("MainWindow", "Symbol:", 0));
        labelReplacementRule->setText(QApplication::translate("MainWindow", "Replacement Rule:", 0));
        labelAxiom->setText(QApplication::translate("MainWindow", "Axiom:", 0));
        pushButtonRoatetNeg->setText(QApplication::translate("MainWindow", "rot -ve", 0));
        pushButtonRotatePos->setText(QApplication::translate("MainWindow", "rot +ve", 0));
        pushButtonBack->setText(QApplication::translate("MainWindow", "Back", 0));
        pushButtonForward->setText(QApplication::translate("MainWindow", "Forward", 0));
        pushButtonPenDown->setText(QApplication::translate("MainWindow", "pen down", 0));
        pushButtonPenUp->setText(QApplication::translate("MainWindow", "pen up", 0));
        pushButtonToggle->setText(QApplication::translate("MainWindow", "pen toggle", 0));
        pushButtonStackPop->setText(QApplication::translate("MainWindow", "pop", 0));
        pushButtonPush->setText(QApplication::translate("MainWindow", "push", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
