#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "TurtleGraphics/python_interface.h"

#include <QMainWindow>

#include "LSystem/LSystem.h"
#include "CommandInterface/CommandInterface.h"
#include "TurtleGraphics/qt_python_syntax_highlighter.h"

#define ADD_SYMBOL_TEXT "Add symbol..."
#define EMPTY_TEXT "(none)"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	LSystem m_lSystem;
    CommandInterface m_commandInterface;
	QtPythonSyntaxHighlighter* m_highlighter = nullptr;


	void updateRules();
	bool loadFile();

	void initPythagorasTree();
	void initKochCurve();
	void initSierpinskiTriangle();
	void initDragonCurve();
	void initFractalPlant();
	void initBoxFractal();
	void initSegmentCurve();
	void initHilbertCurve();
	void initPeanoGosperCurve();
	void initPeanoCurve();
	void initQuadraticKochIsland();

	void pause(const size_t milliseconds);
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_actionSave_triggered();

	void on_actionGenerate_triggered();

	void on_comboBoxIterations_currentIndexChanged(int);

	void on_comboBoxPreset_currentIndexChanged(int index);

	void on_pushButtonRoatetNeg_clicked();

	void on_pushButtonRotatePos_clicked();

	void on_pushButtonPenDown_clicked();

	void on_pushButtonPenUp_clicked();

	void on_pushButtonBack_clicked();

	void on_pushButtonForward_clicked();

	void on_pushButtonToggle_clicked();

	void on_pushButtonStackPop_clicked();

	void on_pushButtonPush_clicked();

	void on_pushButtonExecute_clicked();

    void on_actionOpen_triggered();

	void on_pushButton_clicked();

	void on_pushButton_2_clicked();

private:
	Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
