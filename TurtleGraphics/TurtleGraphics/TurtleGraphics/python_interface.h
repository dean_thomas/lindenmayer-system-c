#ifndef PYTHON_INTERFACE_H
#define PYTHON_INTERFACE_H

#include <Python.h>
#include <string>

class TurtleGraphicsWidget;

class PythonInterface
{
	TurtleGraphicsWidget* m_widget = nullptr;


public:
	explicit PythonInterface(TurtleGraphicsWidget* target);

	PyObject* cursor_reset(PyObject *, PyObject *);

	PyObject* cursor_forward(PyObject *, PyObject *args);
	PyObject* cursor_back(PyObject *, PyObject *args);
	PyObject* cursor_move(PyObject *, PyObject *args);

	PyObject* cursor_rotate_pos(PyObject *, PyObject *args);
	PyObject* cursor_rotate_neg(PyObject *, PyObject *args);
	PyObject* cursor_rotate(PyObject *, PyObject *args);

	PyObject* push(PyObject *, PyObject *);
	PyObject* pop(PyObject *, PyObject *);

	PyObject* pen_up(PyObject *, PyObject *);
	PyObject* pen_down(PyObject *, PyObject *);
	PyObject* pen_toggle(PyObject *, PyObject *);


	void execute_script(const std::string& script);
};

//static PyObject* cursor_forward_handler(PyObject *self, PyObject *args);

#endif // PYTHON_INTERFACE_H
