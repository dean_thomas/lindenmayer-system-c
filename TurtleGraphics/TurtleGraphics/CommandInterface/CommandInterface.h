#ifndef COMMANDINTERFACE_H
#define COMMANDINTERFACE_H

#include <vector>
#include <map>
#include <functional>
#include <ostream>

#include "TurtleGraphics/TurtleGraphicsWidget.h"


class TurtleGraphicsWidget;

class CommandInterface
{
    friend std::ostream& operator << (std::ostream& os, const CommandInterface& commandInterface);
private:
    using NullaryFunction = std::function<void(void)>;

    TurtleGraphicsWidget* m_targetWidget = nullptr;

	public:  
		using Command = std::function<void(void)>;
		struct CommandEntry
		{
				std::string description;
				Command command;
		};
		using CommandDictionary = std::map<char, CommandEntry>;
		using CommandList = std::vector<Command>;

        CommandInterface();

		CommandList parseString(const std::string &input) const;

		void AddCommand(const char key, const std::string& description, const Command& command);
		template <typename T>
		void AddCommand(const char key, const std::string& description, const Command& command, const T param0);
        void SetTargetWidget(TurtleGraphicsWidget* const target);

        void Clear();

        NullaryFunction GenerateMoveCommand(const float distance) const;
        NullaryFunction GenerateTurnCommand(const float degrees) const;
        NullaryFunction GenerateStackCommand(const bool push) const;
        NullaryFunction GenerateMacroCommand(const std::vector<NullaryFunction>& functions) const;
	private:

        bool commandExists(const char key) const;
		Command lookupCommand(const char key) const;

		CommandDictionary m_commandDictionary;
};

#endif // COMMANDINTERFACE_H
