#   Comments start with a hash symbol
#
#   Created on 04-05-2016
#
#   The title will indicate what is display in the user interface
title:  Fractal Plant

#   The axiom represents the start state of the system
axiom:  X

#   Replacement rules are represented one line per rule (and are case sensitive)
X:  F−[[X]+X]+F[+FX]−X
F:  FF
