#include <iostream>
#include "LSystem.h"

using std::cout;

int main()
{
//#define PYTHAGORAS_TREE
//#define KOCH_CURVE
//#define SIERPINSKI_TRIANGLE
//#define DRAGON_CURVE
//#define FRACTAL_PLANT
//#define BOX_FRACTAL
#define SEGMENT_CURVE
	//LSystem algae("Algae","A", { { 'A', "AB" }, { 'B', "A" } });
	//LSystem pythagorasTree("Pythagoras Tree", "0", { { '1', "11" }, { '0', "1[0]0" } });
	//LSystem cantorDust("Cantor Dust", "A", { { 'A', "ABA" }, { 'B', "BBB"} });
	//LSystem kochCurve("Koch Curve", "F", { { 'F', "F+F-F-F+F" } });
	//
	//
	
	/*
	cout << algae.ToString();
	for (unsigned long i = 0; i < 8; ++i)
	{
		cout << i << ": " << algae.Generate(i) << "\n";
	}
	cout << "\n";
	

	cout << pythagorasTree.ToString();
	for (unsigned long i = 0; i < 10; ++i)
	{
		cout << i << ": " << pythagorasTree.Generate(i) << "\n";
	}
	cout << "\n";

	
	cout << cantorDust.ToString();
	for (unsigned long i = 0; i < 4; ++i)
	{
		cout << i << ": " << cantorDust.Generate(i) << "\n";
	}
	cout << "\n";

	cout << kochCurve.ToString();
	for (unsigned long i = 0; i < 4; ++i)
	{
		cout << i << ": " << kochCurve.Generate(i) << "\n";
	}
	cout << "\n";*/
#ifdef SIERPINSKI_TRIANGLE
	LSystem sierpinskiTriangle("Sierpinski Triangle", "A", { { 'A', "+B-A-B+" }, { 'B', "-A+B+A-" } });

	cout << sierpinskiTriangle.ToString();
	for (unsigned long i = 0; i < 7; ++i)
	{
		cout << i << ": " << sierpinskiTriangle.Generate(i) << "\n";
	}
	cout << "\n";
#endif
#ifdef DRAGON_CURVE
	LSystem dragonCurve("Dragon Curve", "FX", { { 'X', "X+YF+" }, { 'Y', "-FX-Y" } });

	cout << dragonCurve.ToString();
	for (unsigned long i = 0; i < 9; ++i)
	{
		cout << i << ": " << dragonCurve.Generate(i) << "\n";
	}
	cout << "\n";
#endif
#ifdef FRACTAL_PLANT
	LSystem fractalPlant("Fractal Plant", "X", { { 'X', "F-[[X]+X]+F[+FX]-X" }, { 'F', "FF" } });

	cout << fractalPlant.ToString();
	for (unsigned long i = 0; i < 4; ++i)
	{
		cout << i << ": " << fractalPlant.Generate(i) << "\n";
	}
#endif
#ifdef BOX_FRACTAL
	LSystem boxFractal("Box Fractal", "F-F-F-F", { { 'F', "F-F+F+F-F" } });

	cout << boxFractal.ToString();
	for (unsigned long i = 0; i < 4; ++i)
	{
		cout << i << ": " << boxFractal.Generate(i) << "\n";
	}
#endif
#ifdef SEGMENT_CURVE
	LSystem segmentCurve("32-Segment Curve Fractal", "F+F+F+F", { { 'F', "-F+F-F-F+F+FF-F+F+FF+F-F-FF+FF-FF+F+F-FF-F-F+FF-F-F+F+F-F+" } });

	cout << segmentCurve.ToString();
	for (unsigned long i = 0; i < 3; ++i)
	{
		cout << i << ": " << segmentCurve.Generate(i) << "\n";
	}
#endif

	cout << "\n";
	getchar();
	return 0;
}