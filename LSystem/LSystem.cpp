#include "LSystem.h"

///
///	\brief	Tests if all characters have a corresponding
///			replacement rules.
///	\since	12-08-2015
///
bool LSystem::validateSystem() const
{
	return true;
}

///
///	\brief	Returns a list of constants in the LSystem,
///			any character that appears but doesn't have
///			an associated replacement rule.
///	\since	12-08-2015
///
vector<char> LSystem::getConstants() const
{
	vector<char> result;

	//	Iterate through the rules
	for (RuleLookupTable::const_iterator itR = m_rules.begin();
		itR != m_rules.end(); ++itR)
	{
		string currentRule = itR->second;

		//	Iterate through the chars in the current string
		for (string::const_iterator itS = currentRule.begin();
			itS != currentRule.end(); ++itS)
		{
			char currentChar = *itS;

			//	Find a rule for the current character
			RuleLookupTable::const_iterator foundChar 
				= m_rules.find(currentChar);

			//	If not found add to the constants list
			if (foundChar == m_rules.end())	
				result.push_back(currentChar);
		}
	}
	return result;
}

///
///	\brief	Initializes the LSystem with a starting axiom.
///	\since	12-08-2015
///
LSystem::LSystem(const string &title, const string &axiom)
: m_title{ title }, m_axiom{ axiom }
{
	
}

///
///	\brief	Initializes the LSystem with a starting axiom,
///			and a set of rules.
///	\since	12-08-2015
///
LSystem::LSystem(const string &title, const string &axiom, const RuleLookupTable &rules)
: LSystem(title, axiom)
{
	m_rules = rules;
}

///
///	\brief	Adds the rule to the internal database.
///	\since	12-08-2015
///
void LSystem::AddRule(const char &character, const string &replacementRule)
{
	//m_rules.push_back(Rule{ character, replacementRule });
	m_rules.insert({ character, replacementRule });
}

///
///	\brief	Destructor.
///	\since	12-08-2015
///
LSystem::~LSystem()
{
}

///
///	\brief	Finds the rule for the given character.
///	\since	12-08-2015
///
string LSystem::lookupRule(const char &character) const
{
	//	Find the rule in the lookup
	RuleLookupTable::const_iterator it = m_rules.find(character);

	if (it != m_rules.end()) 
		//	If a conversion exists return it
		return it->second;
	else 
		//	If not return the input (it was a constant)
		return string(1, character);
}

///
///	\brief	Generates the output for the current system for the
///			given number of iterations.
///	\since	12-08-2015
///
string LSystem::Generate(const long &iterationCount) const
{
	string currentGen = m_axiom;
	
	//	Our temporary store for generation of a new string
	stringstream buffer;

	//	Repeat for the specified number of iterations
	for (unsigned g = 0; g < iterationCount; ++g)
	{
		//	Loop over characters in the existing string
		for (unsigned c = 0; c < currentGen.length(); ++c)
		{
			//	Get the existing character and lookup a
			//	replacement string to be added to the output
			char currentChar = currentGen[c];
			string replacementString = lookupRule(currentChar);
			
			//	Update the new string
			buffer << replacementString;
		}

		//	Set the current generation string to the modified
		//	version and clear the buffer
		currentGen = buffer.str();
		buffer = stringstream();
	}

	return currentGen;
}

///
///	\brief	Return the contents of the LSystem as a string,
///			listing rules and initial axiom.
///	\since	12-08-2015
///
string LSystem::ToString() const
{
	stringstream result;
	vector<char> constants = getConstants();

	result << "LSystem: " << m_title << "\n";

	//	List all variables in the 'alphabet' of the system.
	result << "Variables:";
	for (RuleLookupTable::const_iterator it = m_rules.begin(); 
		it != m_rules.end(); ++it)
	{
		result << " " << it->first;
	}
	result << ";\n";

	//	List all constants in the 'alphabet' of the system.
	result << "Constants:";
	for (unsigned int i = 0; i < constants.size(); ++i)
	{
		result << " " << constants[i];
	}
	result << ";\n";

	//	List the initial starting state of the system.
	result << "Axiom: " << m_axiom << ";\n";

	//	List rules for replacement in the system.
	result << "Rules:";
	for (RuleLookupTable::const_iterator it = m_rules.begin(); 
		it != m_rules.end(); ++it)
	{
		result << " (" << it->first << " -> " << it->second << ")";
	}
	result << ";\n";

	return result.str();
}