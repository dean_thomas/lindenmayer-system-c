#ifndef LSFILE_H
#define LSFILE_H

#include <string>
#include <regex>
#include <utility>
#include <map>

#include "LSystem/LSystem.h"

class LsFileReader
{
    using Rule = std::pair<char, std::string>;
    using RuleList = std::map<char, std::string>;

    std::regex m_commentRegex;
    std::regex m_titleRegex;
    std::regex m_axiomRegex;
    std::regex m_ruleRegex;

    Rule parseRule(const std::smatch& regexMatch) const;
public:
    LsFileReader();
    LSystem operator ()(const std::string& filename);
};


#endif // LSFILE_H
