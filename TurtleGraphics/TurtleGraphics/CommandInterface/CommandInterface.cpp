#include "CommandInterface.h"
#include <cassert>
#include <functional>
#include <iostream>

using namespace std;

void CommandInterface::Clear()
{
    m_commandDictionary.clear();
}

CommandInterface::CommandInterface()
{

}

void CommandInterface::SetTargetWidget(TurtleGraphicsWidget* const target)
{
    assert(target != nullptr);

    m_targetWidget = target;
}

CommandInterface::CommandList CommandInterface::parseString(const std::string& input) const
{
	CommandInterface::CommandList result;

	for (auto& c : input)
	{
        if (commandExists(c)) result.push_back(lookupCommand(c));
	}
	return result;
}

bool CommandInterface::commandExists(const char key) const
{
    auto cIt = m_commandDictionary.find(key);
    return (cIt != m_commandDictionary.cend());
}

CommandInterface::Command CommandInterface::lookupCommand(const char key) const
{
    auto cIt = m_commandDictionary.find(key);
	assert(cIt != m_commandDictionary.cend());

	return cIt->second.command;
}

CommandInterface::NullaryFunction CommandInterface::GenerateStackCommand(const bool push) const
{
    assert(m_targetWidget != nullptr);

    if (push)
    {
        return std::bind(&TurtleGraphicsWidget::push, m_targetWidget);
    }
    else
    {
        return std::bind(&TurtleGraphicsWidget::pop, m_targetWidget);
    }
}

CommandInterface::NullaryFunction CommandInterface::GenerateMoveCommand(const float distance) const
{
    assert(m_targetWidget != nullptr);

    return std::bind(&TurtleGraphicsWidget::moveForward, m_targetWidget, distance);
}

CommandInterface::NullaryFunction CommandInterface::GenerateTurnCommand(const float degrees) const
{
    assert(m_targetWidget != nullptr);

    if (degrees < 0.0f)
    {
        return std::bind(&TurtleGraphicsWidget::rotateCursorNeg, m_targetWidget, -degrees);
    }
    else
    {
        return std::bind(&TurtleGraphicsWidget::rotateCursorPos, m_targetWidget, degrees);
    }
}

void CommandInterface::AddCommand(const char key, const std::string& description, const Command& command)
{
	auto commandEntry = CommandEntry{ description, command };

	m_commandDictionary[key] = commandEntry;
}

std::ostream& operator <<(std::ostream& os, const CommandInterface& commandInterface)
{
    for (auto& command : commandInterface.m_commandDictionary)
    {
        CommandInterface::CommandEntry commandEntry = command.second;

        os << command.first << " : " << commandEntry.description << endl;
    }
    return os;
}

/*
template <typename T>
void CommandInterface::AddCommand(const char key, const std::string& description, const function<void(T)>& command, const T param0)
{
	auto proxy = std::bind(command, param0);
	auto commandEntry = CommandEntry{ description, proxy };

	m_commandDictionary[key] = commandEntry;
}
*/
