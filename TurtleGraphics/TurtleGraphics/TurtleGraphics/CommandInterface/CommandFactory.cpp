#include "CommandFactory.h"

CommandFactory::CommandFactory()
    : CommandFactory(nullptr)
{

}

CommandFactory::CommandFactory(QtTurtleCursor *cursor)
    : m_turtleCursor{cursor}
{

}

CommandFactory::~CommandFactory()
{

}

CommandFactory::PtrCommand CommandFactory::NewMacroCommand(PtrCommand &&existingCommand, PtrCommand &&newCommand)
{
    assert (m_turtleCursor != nullptr);

    return PtrCommand(new MacroCommand(std::move(existingCommand),
                                       std::move(newCommand)));
}

CommandFactory::PtrCommand CommandFactory::NewStackCommand(const StackCommand::Action &action)
{
    assert (m_turtleCursor != nullptr);

    return PtrCommand(new StackCommand(m_turtleCursor, action));
}

CommandFactory::PtrCommand CommandFactory::NewMoveCommand(const MoveCommand::Action &action,
                                                          const float &distance)
{
    assert (m_turtleCursor != nullptr);

    return PtrCommand(new MoveCommand(m_turtleCursor, distance));
}

CommandFactory::PtrCommand CommandFactory::NewRotateCommand(const RotateCommand::Action &action, const float &degrees)
{
    assert (m_turtleCursor != nullptr);

    return PtrCommand(new RotateCommand(m_turtleCursor, action, degrees));
}

CommandFactory::PtrCommand CommandFactory::NewNullCommand()
{
    assert (m_turtleCursor != nullptr);

    return PtrCommand(new NullCommand());
}
