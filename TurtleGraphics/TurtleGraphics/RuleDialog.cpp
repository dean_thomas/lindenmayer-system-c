#include "RuleDialog.h"
#include "ui_RuleDialog.h"

char RuleDialog::GetSelectedCharacter() const
{
    return ui->comboBoxSymbol->currentText().toStdString()[0];
}

string RuleDialog::GetReplacementString() const
{
    return ui->lineEditReplacementString->text().toStdString();
}

RuleDialog::RuleDialog(const LSystem &lSystem, QWidget *parent) :
    QDialog(parent),
    m_lSystem{lSystem},
    ui(new Ui::RuleDialog)
{
    ui->setupUi(this);

    //assert(m_lSystem != nullptr);

    ui->comboBoxSymbol->clear();
    vector<char> chars = lSystem.GetAvailableSymbols();
    for (vector<char>::const_iterator ch = chars.begin();
         ch != chars.end(); ++ch)
    {
        ui->comboBoxSymbol->addItem(QString(*ch));
    }
}

RuleDialog::~RuleDialog()
{
    delete ui;
}
