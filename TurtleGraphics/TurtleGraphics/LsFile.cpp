#include "LsFile.h"

#include <regex>
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;

///
/// \brief LsFileReader::LsFileReader
/// \since  04-05-2016
/// \author Dean
///
LsFileReader::LsFileReader()
{
    m_commentRegex = R"(\s*#(.*))";
    m_titleRegex = R"(\s*title:\s*(.*))";
    m_axiomRegex = R"(\s*axiom:\s*(.*))";
    m_ruleRegex = R"(\s*(.)\s*:\s*(.*))";
}

///
/// \brief LsFileReader::parseRule
/// \param regexMatch
/// \return
/// \since  04-05-2016
/// \author Dean
///
LsFileReader::Rule LsFileReader::parseRule(const std::smatch& regexMatch) const
{
    assert(regexMatch.size() > 2);

    return make_pair(regexMatch[1].str()[0], regexMatch[2].str());
}

///
/// \brief LsFileReader::operator ()
/// \param filename
/// \return
/// \since  04-05-2016
/// \author Dean
///
LSystem LsFileReader::operator ()(const std::string& filename)
{
    ifstream inputFile(filename);
    assert(inputFile.is_open() && inputFile.good());

    LSystem result;

    while (!inputFile.eof())
    {
        string buffer;
        getline(inputFile, buffer);

        //cout << buffer << endl;

        std::smatch regexMatches;
        if (regex_match(buffer, regexMatches, m_commentRegex))
        {
            cout << "Comment: ";
            for (auto it = regexMatches.cbegin()+1; it != regexMatches.cend(); ++it)
            {
                cout << it->str();
            }
            cout << endl;
        }
        else if (regex_match(buffer, regexMatches, m_titleRegex))
        {
            assert(regexMatches.size() > 1);
            result.SetTitle(regexMatches[1].str());
            cout << "System title: " << result.GetTitle() << endl;
        }
        else if (regex_match(buffer, regexMatches, m_axiomRegex))
        {
            assert(regexMatches.size() > 1);
            result.SetAxiom(regexMatches[1].str());
            cout << "axiom: " << result.GetAxiom() << endl;
        }
        else if (regex_match(buffer, regexMatches, m_ruleRegex))
        {
            auto rule = parseRule(regexMatches);
            result.AddRule(rule.first, rule.second);
            cout << "rule: " << rule.first << " -> " << rule.second << endl;
        }
        else
        {
            if (buffer != "") cerr << "Unrecognised text in file: " << buffer << endl;
        }
    }

    inputFile.close();

    assert(result.IsValid());
    return result;
}
